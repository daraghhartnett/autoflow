# First version of the Summer 2020 SRI TA@ Base images for the Dry Run 
FROM registry.gitlab.com/daraghhartnett/tpot-ta2:master

RUN mkdir -p /user_dev/autoflow
RUN mkdir -p /output
RUN mkdir -p /input
RUN mkdir -p /work

COPY d3mStart.sh /user_dev/autoflow
RUN  chmod a+x /user_dev/autoflow/d3mStart.sh
ENTRYPOINT ["/user_dev/autoflow/d3mStart.sh"]

